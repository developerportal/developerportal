<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->


<!-- Mirrored from demo.geekslabs.com/materialize/v3.1/layout-horizontal-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Feb 2016 10:25:19 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Developer portal .Solution to a scenario ">
    <meta name="keywords" content="plugin,api,java,php,developer,portal,india,developerportal,developerportal.in,developerport,develortal,html,html5,www.developerportal.in,css,css5,spring,stack,tutorial,plugins,api's,hibernate,j2ee,corejava,jquery,scenario,solution,script,javascript,JS,JQ,core java,itext,thief,colorthief,dominator">
    <title>Developer Portal |solution to a scenario</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->    
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS style Horizontal Nav-->    
    <link href="css/layouts/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body id="layouts-horizontal" style="overflow-y: scroll;width: 99%;">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color z-depth-0">
                <div class="nav-wrapper teal darken-2">
                    <ul class="left">                      
                      <li><h1 class="logo-wrapper"><a href="index-2.html" class="brand-logo darken-1">
                         
                          <a class="btn-floating btn-large waves-effect waves-light blue darken-1"><i class="">D</i></a> <span class="main">Developer Portal<span style="position: relative;float: inherit; font-size: 14px;top: 15px;right: 120px;">Solution to a scenario</span></span>
                      </a></h1></li>
                    </ul>
                   
                    <ul class="right hide-on-small-and-down" style="margin: 17px">
                        
                                             
                        <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i style="font-size: 2em;"  class="mdi-device-access-time"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" data-activates="chat-out" class="waves-effect waves-block waves-light translation-button" style="font-size: 1.2em;"  data-activates="translation-dropdown">Wed ,10/04/2016 10:23</a>
                        </li>
                    </ul>
                 
                </div>
            </nav>

           
            
            

        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
         
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                    <div id="card-widgets">
                       <div class="row">
                  <div class="col s12 m12 l12 no-padding darken-1" style="height:47px">
                    <ul class="tabs tab-demo z-depth-1 teal " style="width: 101%;height: 47px;z-index: 100;position: fixed;    /*margin: 0 4px*/">
                      <li class="tab col s3"><a class="active white-text" href="#test1"><i class="mdi-action-receipt tab-icon" > </i> <i class="tab-icon-text">Java API's</i> </a>
                      </li>
                      <li class="tab col s3"><a href="#test2" class="white-text"><i class="mdi-action-history tab-icon"></i> <i class="tab-icon-text"> Tell Us UR Scenario</i></a>
                      </li>
                      <li class="tab col s3"><a href="#test3" class="white-text"><i class="mdi-action-search tab-icon"></i> <i class="tab-icon-text"> Search </i></a>
                      </li>
                      <li class="tab col s3"><a href="#test4" class="white-text"><i class="mdi-file-file-upload tab-icon"></i>  <i class="tab-icon-text"> Upload </i></a>
                      </li>
                    <div class="indicator" style="right: 1021px; left: 0px;"></div><div class="indicator" style="right: 1021px; left: 0px;"></div></ul>
                  </div>
                  <div class="col s12 tab-data-container" >
                    <div id="test1" class="col s12" style="display: block;margin-left:7px">
                     <div class="row">
                            <div class="col s12 m12 l4 card-type">
                                <div class="card small">
                                 <div class="card-action">
                                    <a href="http://itextpdf.com/" class="right teal-text">Automated Pdf</a>
                                  </div>
                                  <div class="card-image">
                                    <img src="https://pbs.twimg.com/profile_images/515066092746596352/GI1n20xs_400x400.png" class="background">
                                    <span class="card-title blue-text text-darken-3">Automated PDF <i class="mdi-action-opensource"></i></span>
                                      <img src="images/open_source_128_px.png" class="info-icon">
                                  </div>
                                  <div class="card-content">

                                    <p>Standards for administration, archiving, invoicing and compliance increasingly rely on PDF. iText is the right partner to help you automate your documentation processes.....</p>
                                  </div>
                                  <div class="card-action" style="background: linear-gradient(to bottom,#FF9E19,white)">
                                    <a href="http://itextpdf.com/" target="_blanck" class="right teal-text" >Visit iText</a>
                                  </div>
                                </div>
                                <div class="card small">
                                  <div class="card-image">
                                    <img class="" src="images/sample-1.jpg">
                                    <span class="card-title">Card Title</span>
                                  </div>
                                  <div class="card-content">
                                    <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#">This is a link</a>
                                    <a href="#">This is a link</a>
                                  </div>
                                </div>
                                <div class="card small">
                                  <div class="card-image">
                                    <img src="images/sample-1.jpg">
                                    <span class="card-title">Card Title</span>
                                  </div>
                                  <div class="card-content">
                                    <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#">This is a link</a>
                                    <a href="#">This is a link</a>
                                  </div>
                                </div>
                            </div>
                            
                            <div class="col s12 m12 l4 card-type">
                                <div class="card small">
                                  <div class="card-image">
                                    <img src="images/sample-1.jpg">
                                    <span class="card-title">Card Title</span>
                                  </div>
                                  <div class="card-content">
                                    <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#">This is a link</a>
                                    <a href="#">This is a link</a>
                                  </div>
                                </div>
                   
                            </div>
                            
                            <div class="col s12 m12 l4 card-type">
                                <div class="card small">
                                  <div class="card-image">
                                    <img src="images/sample-1.jpg">
                                    <span class="card-title">Card Title</span>
                                  </div>
                                  <div class="card-content">
                                    <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#">This is a link</a>
                                    <a href="#">This is a link</a>
                                  </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="test2" class="col s12" style="display: none;">
                      <dl>
                        <dt>Definition list</dt>
                        <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</dd>
                        <dt>Lorem ipsum dolor sit amet</dt>
                        <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</dd>
                      </dl>
                    </div>
                    <div id="test3" class="col s12" style="display: none;">
                      <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies
                        mi vitae est. Mauris placerat eleifend leo.</p>
                    </div>
                    <div id="test4" class="col s12" style="display: none;">
                      <ul>
                        <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                        <li>Aliquam tincidunt mauris eu risus.</li>
                        <li>Vestibulum auctor dapibus neque.</li>
                      </ul>
                    </div>
                  </div>
                </div>
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START RIGHT SIDEBAR NAV-->
           
            <!-- LEFT RIGHT SIDEBAR NAV-->

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->


    <!-- //////////////////////////////////////////////////////////////////////////// -->

  
     <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">World Market</h5>
                    <p class="grey-text text-lighten-4">World map, world regions, countries and cities.</p>
                    <div id="world-map-markers"></div>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Sales by Country</h5>
                    <p class="grey-text text-lighten-4">A sample polar chart to show sales by country.</p>
                    <div id="polar-chart-holder">
                        <canvas id="polar-chart-country" width="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
       <div class="footer-copyright">
            <div class="container">
                Copyright © 2016 <a class="grey-text text-lighten-4" href="" target="_blank">gfddf</a>dfgdfg
                <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="fgfg">Ganesh Bbu HC</a></span>
            </div>
        </div>
    </footer> 
 


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    <!-- Toast Notification -->
    <script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
       /* setTimeout(function() {
            Materialize.toast('<span>Hiya! I am a toast.</span>', 1500);
        }, 1500);
        setTimeout(function() {
            Materialize.toast('<span>You can swipe me too!</span>', 3000);
        }, 5000);
        setTimeout(function() {
            Materialize.toast('<span>You have new order.</span><a class="btn-flat yellow-text" href="#">Read<a>', 3000);
        }, 15000);*/
    });
    </script>
</body>

</html>